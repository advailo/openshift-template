# OpenShift/OKD template project
## Setting environment variables
All commands are executed from mgmt machine which is logged in with admin rights to OpenShift/OKD cluster

For esier use I'm setting environment variables:
```
export OCP_NAMESPACE=template_project
export OCP_APP=template_app
```
## Create project
```
oc new-project $OCP_NAMESPACE
```
## Create service account for CircleCI
- Create service account for CircleCI
- Give edit role to namespace
- Get token (For CircleCI to access Openshift/OKD cluster)
```
oc create sa circleci
oc policy add-role-to-user edit system:serviceaccount:$OCP_NAMESPACE:circleci
oc sa get-token circleci
```
## Create deployment ssh key
Creating ssh key for openshift build to access private git repo. Private key is added to OpenShift/OKD secret and public key is displayed (Need to add it to your git repo as deployment key)
```
ssh-keygen -f /tmp/git-$OCP_APP -N '' -C git-$OCP_APP
cat /tmp/git-$OCP_APP.pub
oc create secret generic git-$OCP_APP --from-file=ssh-privatekey=/tmp/git-$OCP_APP --type=kubernetes.io/ssh-auth
rm /tmp/git-$OCP_APP /tmp/git-$OCP_APP.pub
```

## CircleCI configuration
Need to set environment variables
```
OCP_URL=...
OCP_TOKEN=...
OCP_NAMESPACE=...
OCP_APP=...
```
## OpenShift/OKD configuration
- Copy `.circleci` and `openshift` folders from this repo
- Add ca.pem from OpenShift/OKD cluster as file `openshift/ca.pem`
- Set git url in `openshift/buildconfig.yml`